---
title: Contact
menu:
  main:
    name: Contact
    weight: 20
aliases:
  - /contact.html
lastmod: 2022-04-25T12:28:34.186Z
---

{{% blocks/lead color="primary" %}}
# Find us

### Source control

We started out using [GitHub](https://github.com/thegooddocsproject).

In October 2022, we migrated to [Gitlab](https://gitlab.com/tgdp) and archived our GitHub repos. You can read more about our decision to migrate in [RFC #009 - Migrate to GitLab](https://gitlab.com/tgdp/request-for-comment/-/blob/main/Accepted-RFCs/RFC-009-migrate-to-gitlab.md).

### Slack

We regularly chat in Slack. You can join [our Slack workspace](https://join.slack.com/t/thegooddocs/shared_invite/zt-be2gay0m-Ukq_5SI0MHp20IQP3auQjg).

The main channels are:

* #general: General discussions for everyone in the group
* #welcome: Join this channel and introduce yourself! We respond.

### Twitter

We tweet from [@thegooddocs](https://twitter.com/thegooddocs)

### Email lists

We have email lists, but don't use them much. You can join from our [groups.io](https://thegooddocsproject.groups.io/g/main) mailing list admin page.



{{% /blocks/section %}}
