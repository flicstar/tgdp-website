---
title: Write the Docs Portland 2022 Conference Highlights
date: 2022-07-16T16:00:00Z
publishDate: null
lastmod: 2022-07-16T01:10:56.446Z
image: /uploads/blog/headers/portland-over.png
images:
  - /uploads/blog/headers/portland-over.png
author:
  - felicity_brand
tags:
  - the-project
custom_copyright: " "
custom_license: " "
type: blog
---
One of our fave conferences is over for another year, and we want to take a moment to celebrate it. The [Write the Docs Portland conference](https://www.writethedocs.org/conf/portland/2022/news/thanks-recap/), held in May, was a lively and successful event, abuzz with energy (as usual). 

Conferences are always a good opportunity for us to mix with our peers, showcase what we’ve been working on, and entice new people to join our project. 

Here at The Good Docs Project, we use the Write the Docs international conferences as milestones in our yearly planner. They are markers for our releases, as well as great reasons to get stuff done in our own project! We also love to attend them, and participate as much as we can. At the [Write the Docs Portland](https://www.writethedocs.org/conf/portland/2022/) conference, we had a presence at Writing day, speakers at the conference, and we hosted an unconference table.

We want to send a big thanks and shout-out to Write the Docs for organizing the conference for another year. Here’s a recap of (some of) what we did and our favorite bits. 


## Writing Day
We scheduled a [full day’s worth of activities](https://www.writethedocs.org/conf/portland/2022/writing-day/#the-good-docs-project) for Writing Day. Each activity was an hour long, led by a different person and covered a different aspect of The Good Docs Project. Our aims were to give tech writers hands-on experience contributing to various aspect of our project, to build our user base and to grow our community. 

> “I liked that we were able to have different leaders for each session. Not only did it spread the load of organizing, it also demonstrated the depth of 
> talent and commitment within our community.”—Cameron Shorter

We had hoped to get more “real work” done to progress some of our tasks but instead we had many lively discussions, and received a stack of valuable feedback we are now incorporating into our templates and processes.


## Conference Days
We had five community members from The Good Docs Project speak at the conference. As expected, presentations and lightning talks spawned hallway chat and private connections, prompting writers to reach out and find out more about the project. 

<i class="fab fa-youtube"></i> See the [full playlist](https://www.youtube.com/playlist?list=PLZAeFn6dfHpnDhFvXG8GprqlLlzSQRBui) on YouTube.

### Peer writing and beyond

Chris Ganta presented [Peer writing and beyond - An experimental approach to a sustainable open-source projects](https://www.writethedocs.org/conf/portland/2022/speakers/#speaker-chris-ganta), developed in conjunction with Gayathri Krishnaswamy and Nelson Guya.

The talk highlights their experience developing the quickstart template within The Good Docs Project. Great work team! 

{{< youtube Q_Wcwzz8x5M >}}

</br>

### Lightning talks

Several community members were moved to submit lightning talks about their own personal passions. We loved hearing their voices and we love that our project provides a space for people to be able to work with them.



* **Marketing and Documentation Site with Gitpod, Frontmatter CMS and Hugo SSG**

   Bryan Klein gave a great lightning talk about some tools that we use here in The Good Docs Project, like Gitpod and Frontmatter.  

   {{< youtube T7neqUfnVvo >}}

   </br>
 

* **5 steps to a great quickstart**

  Gayathri Krishnaswamy showed us why they’re called lightning talks - compressing all the lessons learned when building the quickstarts template into 5 minutes.
 
  {{< youtube V-tZo0rArag >}}

   </br>

* **Can I interest you in Empathy Advocacy?**

  Ryan Macklin put the call out, wanting to share his knowledge and seeking folks to take up the torch for empathy advocacy.  
  
  {{< youtube xfnlNxq0V_A >}}

   </br>


## Unconference Session

Carrie Crowe and Felicity Brand held an unconference session about Content Strategy. We intentionally wanted this to be an unstructured discussion covering the goals of the content strategy working group. We kept it loose and flexible, and the session shifted toward a more general conversation about the project, how we work and how much time we commit. We took some learnings away from this session for next time (perhaps we'll introduce more structure?). 


## Social event

It’s wonderful to have a more informal session after the more structured aspects of the conference. The [SpatialChat](https://spatial.chat/) platform was super fun. Attendees were able to move around the virtual room, listening to nearby conversations and hearing whispers from chats further away. 

> "I loved the social session. It was just like being in a crowded social event in a ballroom! You could move closer to chats that interested you. Almost better than an 
> IRL event!"—Felicity Brand 

This event was a great opportunity to make deeper connections, get some perspectives on day-to-day work challenges, find out about people’s pets and just be plain silly. 


## See you next time

At Write the Docs conferences we always meet loads of new people and get to welcome them into our community, so yay! We met a lot of people who expressed an interest in joining The Good Docs Project - we had 34 responses on our newcomer form, and we welcomed an influx of new contributors to the project.

> “For me, the big success was the number of new community members who found us”—Alyssa Rock

Tell us what you loved about the conference in the comments, and be sure to mark your calendar for [Write the Docs Prague](https://www.writethedocs.org/conf/prague/2022/) in October 2022.
