---
title: Tina Luedtke
image: /uploads/blog/authors/tina_luedtke.jpeg
description: Tech writer by training. Nerd at heart.
follow: Find Tina on [Twitter](https://twitter.com/kickoke)
web: www.kickoke.com
email: kickoke@gmail.com
social:
  github: kickoke
  gitlab: kickoke
  linkedin: tina-luedtke
  medium: ""
  twitter: kickoke
  instagram: ""
type: author
lastmod: 2022-07-18T19:32:04.580Z
draft: false
---

Tina Luedtke (aka kickoke) is a technical writer who is in the heart of Silicon Valley. She loves thinking about content strategy, information architecture and personalized content delivery. During the day, she crafts documentation for Google. By night, you can find her discussing content reuse and knowledge graphs with fellow tech writers at Write the Docs Meetups.
{class="lead" }

Tina leads the Chronologue project, a working group within The Good Docs Project. The main goal of the Chronologue group is to illustrate what good documentation looks like in action - without using "foo" and "bar". The group writes documentation for a fictional time-travel telescope that records events from the future and past, and provides those recordings on the Chronologue website. 
