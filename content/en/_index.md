---
title: The Good Docs Project
linkTitle: Home
lastmod: 2022-09-26T17:12:57.617Z
---

{{< blocks/cover title="The Good Docs Project" image_anchor="top" height="max" color="orange" >}}

<div class="mx-auto">
<p class="lead mt-5">Best practice templates and writing instructions for documenting open source software.</p>
  <h6>  
    Photo by <a href="https://unsplash.com/@patrickian4?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Patrick Fore</a> on <a href="https://unsplash.com/s/photos/technical-writer?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
  </h6>
	<span>{{< blocks/link-down color="info" >}}</span>
</div>

{{< /blocks/cover >}}


{{% blocks/section type="section" color="white" %}}



<div class="col-12 text-center">
  <a href="https://www.netlify.com">
    <img src="https://www.netlify.com/img/global/badges/netlify-dark.svg" alt="Deploys by Netlify" />
  </a>
</div>

{{< /blocks/section >}}
